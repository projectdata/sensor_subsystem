/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: DATAbus_slave.h

*/

#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"
#include "stm32f10x.h"

#endif

#ifndef DATABUS_SLAVE
#define DATABUS_SLAVE


#define MY_DATAbus_ID 0x10


#define DATABUS_BUFFER_SIZE 1100
#define DATABUS_BUFFER_COUNT 3

#define DATABUS_TIMEOUT 500
#define DATAbus_RX_TIMEOUT 2
#define DATABUS_RESPONSE_DELAY 0

#define INVALID_PACKET 0
#define BROADCAST_RESPONSE 1
#define BROADCAST_REQUEST 2
#define WRITE_REQUEST 3
#define WRITE_RESPONSE 4
#define ECU_MISMATCH 5

#define DATAbus_BROADCAST_REQUEST 0x01
#define DATAbus_BROADCAST_RESPONSE 0x00
#define DATAbus_WRITE_REQUEST 0x03
#define DATAbus_WRITE_RESPONSE 0x02

#define DATAbus_STATE_OFF 0
#define DATAbus_STATE_IDLE 1
#define DATAbus_STATE_AWAITING_RESPONSE 2
#define DATAbus_STATE_VALIDATING_RESPONSE 3
#define DATAbus_STATE_SEND_PACKET 4
#define DATAbus_STATE_BROADCAST_TABLE 5
#define DATAbus_STATE_TIMEOUT 6

struct DATAbus_PACKET
{
	uint8_t start_char;
	uint8_t control_char;
	uint8_t ECU_ID;
	uint8_t len_H;
	uint8_t len_L;
	uint8_t data[1050];
	uint64_t checksum;
};

struct DATAbus_SLAVE_STATUS
{
	uint8_t state;
	uint8_t current_node;
	uint16_t timeout_counter;
};

/**
Initializes the DATAbus master
*/
void init_DATAbus_Slave(void);

/**
Updates the internal state of the DATAbus master
*/
void update_DATAbus_Slave(void);

/**
Adds a character to the RX buffer 
*/
void bus_Slave_RX_Buffer_Add(uint8_t data);

/**
Resets the RX buffer
*/
void bus_Slave_RX_Buffer_Reset(void);

/**
Checks the contents of the received packet and returns the result
*/
uint8_t bus_Slave_Validate_Packet(void);
	
/**
Sends a broadcast request to the specified node on the DATAbus
*/
void bus_Slave_Send_Broadcast_Request(uint8_t ECU_ID);

/**
Sends a broadcast response with the specified ID on the DATAbus
*/
void bus_Slave_Send_Broadcast_Response(struct DATAbus_PACKET* packet);

/**
Sends a write request to the specified node on the DATAbus
*/
void bus_Slave_Send_Write_Request(struct DATAbus_PACKET* packet);

/**
Sends a write response with the specfied ID on the DATAbus
*/
void bus_Slave_Send_Write_Response(uint8_t ECU_ID, uint8_t response);
	
/**
Gets the internal state of the DATAbus state machine
*/
uint8_t get_DATAbus_Slave_State(void);

#endif
