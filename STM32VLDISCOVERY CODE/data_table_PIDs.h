/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: data_table_PIDs.h

*/

#ifndef DATA_TABLE_H
#define DATA_TABLE_H

//Controller Type and Version
#define CONTROLLER_TYPE_PID 2
#define CONTROLLER_SW_VERS_PID 1
#define CONTROLLER_HW_VERS_PID 2

//Serial Number
#define CONTROLLER_SN_LL_PID 11
#define CONTROLLER_SN_LH_PID 12
#define CONTROLLER_SN_HL_PID 13
#define CONTROLLER_SN_HH_PID 14

//Clock
#define UTC_TIME_L_PID 8
#define UTC_TIME_H_PID 9

//CAN Status
#define BUS_MESSAGE_TX_COUNT_L_PID 10
#define BUS_MESSAGE_TX_COUNT_H_PID 11
#define BUS_MESSAGE_RX_COUNT_L_PID 12
#define BUS_MESSAGE_RX_COUNT_H_PID 13

//Controller Status
#define CONTROLLER_POWER_STATUS_PID 14
#define CONTROLLER_TEMP_STATUS_PID 15

//Error Status
#define ACTIVE_ERRORS_STATUS_L_PID 18
#define ACTIVE_ERRORS_STATUS_H_PID 19

//Error Table
#define ERROR_CODE_VECTOR_BASE_PID 20

#endif
