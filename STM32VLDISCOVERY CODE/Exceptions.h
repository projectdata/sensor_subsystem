/** 

Project: Project DATA
Developer: Austin Gosling
Creation Date: 02/14/2018

Subsytem: Main ECU
File: Exceptions.h

*/

#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
#include "stdint.h"
#include "stm32f10x.h"
#include "data.h"

#endif

#include "ErrorCodes.h"

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#define ERROR_CRITICAL_PRIORITY 0x00
#define ERROR_WARNING_PRIORITY 0x01
#define ERROR_ERROR_PRIORITY 0x02
#define ERROR_INFO_PRIORITY 0x03

/**
Adds the error to the error list

Returns the position in the table where the error was added
*/
uint8_t add_Error(uint16_t majorcode, uint8_t minorcode, uint8_t priority);

/**
Removes/de-asserts error at the specified address
*/
void remove_Error(uint8_t position);
	
#endif
