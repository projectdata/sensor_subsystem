/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: data.c

*/

#include "data.h"

struct ECU_TABLE sensor_ECU;

uint16_t transmit_pos = 0;

void init_Table(void)
{
	sensor_ECU.len = 100;
	sensor_ECU.controlchar = 0x55AA;
	sensor_ECU.check = 0x55AA;
	sensor_ECU.ECU_ID = MY_DATAbus_ID;
	
	set_Data(INIT_CONTROLLER_TYPE, CONTROLLER_TYPE_PID);
	set_Data(INIT_CONTROLLER_SW_VERS, CONTROLLER_SW_VERS_PID);
	set_Data(INIT_CONTROLLER_HW_VERS, CONTROLLER_HW_VERS_PID);
	
	set_Data(INIT_CONTROLLER_SN_LL, CONTROLLER_SN_LL_PID);
	set_Data(INIT_CONTROLLER_SN_LH, CONTROLLER_SN_LH_PID);
	set_Data(INIT_CONTROLLER_SN_HL, CONTROLLER_SN_HL_PID);
	set_Data(INIT_CONTROLLER_SN_HH, CONTROLLER_SN_HH_PID);
}

uint16_t get_Data(uint16_t PID)
{
	if(PID < TABLE_SIZE - 1)
	{
		return sensor_ECU.data_table[PID];
	}
	
	return 0;
}

void set_Data(uint16_t data,uint16_t PID)
{
	if(PID < TABLE_SIZE - 1)
	{
		sensor_ECU.data_table[PID] = data;
	}
}

void increment_Data(uint8_t rollover, uint16_t PID)
{
	if(PID < TABLE_SIZE - 1)
	{
		uint16_t last = sensor_ECU.data_table[PID];				//Capture the current value at the PID
		
		sensor_ECU.data_table[PID]++;											//Increment the variable
		
		if(sensor_ECU.data_table[PID] < last && rollover != 0)
		{
			sensor_ECU.data_table[PID + 1]++;								//Increment the value at the next PID address if the rollover bit is set and the lower address overflows
		}
	}
}

uint16_t transmit_Data(struct DATAbus_PACKET* packet)
{
	uint16_t i = 0;
	
	if((transmit_pos * PACKET_SIZE) > MAX_ADDRESS)
	{
		transmit_pos = 0;
	}
	
	if(((transmit_pos + 1) * PACKET_SIZE) - 1 > MAX_ADDRESS)
	{
		sensor_ECU.len = (((transmit_pos + 1) * PACKET_SIZE) - 1) - MAX_ADDRESS;
	}
	else
	{
		sensor_ECU.len = PACKET_SIZE;
	}
	
	sensor_ECU.start = transmit_pos * PACKET_SIZE;
	transmit_pos++;
	
	packet->data[0] = (sensor_ECU.controlchar & 0x00FF);				//Place the packet control character into the data buffer
	packet->data[1] = (sensor_ECU.controlchar >> 8) & 0x00FF;
	packet->data[2] = (sensor_ECU.ECU_ID & 0x00FF);							//Place the ECU_ID into the data buffer
	packet->data[3] = (sensor_ECU.ECU_ID >> 8) & 0x00F;
	packet->data[4] = (sensor_ECU.start & 0x00FF);							//Place the start address of the data into the data buffer
	packet->data[5] = (sensor_ECU.start >> 8) & 0x00FF;
	packet->data[6] = (sensor_ECU.len * 2 & 0x00FF);						//Place the length of the data into the data buffer
	packet->data[7] = (sensor_ECU.len * 2 & 0xFF00) >> 8;
	
	for(i = 0; i < sensor_ECU.len; i++)													//Place the data inot the data buffer
	{
		packet->data[8 + i*2] = (sensor_ECU.data_table[sensor_ECU.start + i] & 0x00FF);
		packet->data[8 + i*2 + 1] = (sensor_ECU.data_table[sensor_ECU.start + i] & 0xFF00) >> 8;
	}
	
	packet->data[8+ i*2] = 0xAA;															//Place the check character at the end of the data
	packet->data[8+ i*2 + 1] = 0x55;
	
	return (sensor_ECU.len * 2) + ECU_TABLE_OVERHEAD;
}
