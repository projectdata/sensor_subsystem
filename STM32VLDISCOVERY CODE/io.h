/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: io.h

*/

#include <stdint.h>
#include "stm32f10x.h"

static int TIM5_counter = 0;
static int TIM5_flag = 0;

void IO_init(void);

void tim_output_init(void);

void tim_input_init(void);

void TIM5_init(void);
