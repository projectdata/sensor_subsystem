/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: data.h

*/

#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"
#include "stm32f10x.h"
#include "DATAbus_slave.h"
#include "comms_slave.h"

#endif

#ifndef DATA_H
#define DATA_H

#include "data_table_PIDs.h"
#include "table_init.h"

#define TABLE_SIZE  (uint16_t)512

/**
The global controller data table for
*/

#define ECU_TABLE_OVERHEAD 10
#define MAX_ADDRESS 122
#define PACKET_SIZE 50

struct ECU_TABLE
{
	uint16_t controlchar;
	uint16_t ECU_ID;
	uint16_t start;
	uint16_t len;
	uint16_t data_table[TABLE_SIZE];
	uint16_t check;
};

//LED Addresses
#define STATUS_LED_1_PID			100
#define STATUS_LED_2_PID			101

//Ultrasonic Sensor Addresses
#define ULTRASONIC_BASE_PID		102
#define	ULTRASONIC_RAW_1_PID	102
#define	ULTRASONIC_CALC_1_PID	103
#define	ULTRASONIC_RAW_2_PID	104
#define	ULTRASONIC_CALC_2_PID	105
#define	ULTRASONIC_RAW_3_PID	106
#define	ULTRASONIC_CALC_3_PID	107
#define	ULTRASONIC_RAW_4_PID	108
#define	ULTRASONIC_CALC_4_PID	109
#define	ULTRASONIC_RAW_5_PID	110
#define	ULTRASONIC_CALC_5_PID	111

//Bump Sensor Addresses
#define BUMP_BASE_PID					112
#define BUMP_RIGHT_STATUS_PID	112
#define BUMP_LEFT_STATUS_PID	114
#define BUMP_FRONT_STATUS_PID 116
#define BUMP_BACK_STATUS_PID	118

//Tilt Sensor Addresses
#define TILT_BASE_PID					120
#define TILT_STATUS_1_PID			120
#define TILT_STATUS_2_PID			122

/** 
Populates the data table with initial values
*/
void init_Table(void);

/**
Gets data from the table at the specified PID 
*/
uint16_t get_Data(uint16_t PID);

/**
Puts data into the table at the specifed PID
*/
void set_Data(uint16_t data, uint16_t PID);

/**
Increments the data at the specified PID, overflows into PID + 1 if rollover != 0
*/
void increment_Data(uint8_t rollover, uint16_t PID);

/**
Places data from the table into the DATAbus packet
*/
uint16_t transmit_Data(struct DATAbus_PACKET* packet);

#endif
