/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: clocks.h

*/

#include <stdint.h>
#include "stm32f10x.h"

//Initialize the Cortex M3 clock using the RCC registers
void clock_init(void);

//Delay the system
void delay(uint32_t);
