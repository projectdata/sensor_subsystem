/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: DATAbus_slave.c

*/

#include "DATAbus_slave.h"
#include "comms_slave.h"
#include "data.h"

struct DATAbus_SLAVE_STATUS status;
struct DATAbus_PACKET control_packet;

uint8_t DATAbus_RX_Buffer[DATABUS_BUFFER_COUNT][DATABUS_BUFFER_SIZE];
uint8_t DATAbus_RX_Pos = 0;
uint8_t DATAbus_RX_Buffer_Active = 0;
uint16_t DATAbus_RX_len = 0;
uint8_t DATAbus_RX_Ready = 0;
uint8_t DATAbus_skip = 0;
uint8_t DATAbus_Validate_Active = 0;

uint8_t DATAbus_ECU_ID = MY_DATAbus_ID;

uint16_t DATAbus_Response_Delay = 0;

uint8_t data_to_send = 0;

uint16_t RX_timeout = 0;

void init_DATAbus_Slave(void)
{
	status.state = DATAbus_STATE_OFF;							//Put the master in the off state
	status.current_node = 0;				//Set schedule position to 0
	status.timeout_counter = 0;		//Set timeout to 10 seconds
	
	status.state = DATAbus_STATE_IDLE;
}


void update_DATAbus_Slave(void)
{
	status.timeout_counter++;	//Increment the timeout counter
	RX_timeout ++;

	if(RX_timeout >= DATAbus_RX_TIMEOUT && DATAbus_RX_Pos != 0)
	{
		DATAbus_RX_Ready = 0x01;
	}
	
	if((status.timeout_counter >= DATABUS_TIMEOUT) && status.state != DATAbus_STATE_OFF)	//If the timeout has been reached then skip the rest of the processing and give the next node its turn
	{
		status.state = DATAbus_STATE_TIMEOUT;
	}

	if(DATAbus_skip == 0)
	{
		uint8_t response = ECU_MISMATCH;
		
		DATAbus_skip = 1;
		switch(status.state)
		{
			case DATAbus_STATE_IDLE:
					if(DATAbus_RX_Ready == 0x01)								//Check to see if a packet has been received
					{
						DATAbus_RX_Ready = 0;
						status.timeout_counter = 0;
						
						response = bus_Slave_Validate_Packet();
						
						if(response == BROADCAST_REQUEST)	//Check to see if the packet is a brodcast request
						{
							GPIOC->ODR |= 0x00000200;				//Turn on status led to indicate start of turn/activity
							
							if(data_to_send)								//If there is data packets to be sent then send them now
							{
								status.state = DATAbus_STATE_SEND_PACKET;		
							}
							else														//If not then broadcast my table and end my turn
							{
								status.state = DATAbus_STATE_BROADCAST_TABLE;
							}
						}
					}
			break;
			case DATAbus_STATE_SEND_PACKET:
				if(DATAbus_Response_Delay >= DATABUS_RESPONSE_DELAY)
				{
					DATAbus_Response_Delay = 0;
					
					data_to_send = 0;
					status.timeout_counter = 0;											//Clear the timeout counter
					status.state = DATAbus_STATE_AWAITING_RESPONSE;	
				}
				else
				{
					DATAbus_Response_Delay++;
				}
			break;
			case DATAbus_STATE_AWAITING_RESPONSE:
					if(DATAbus_RX_Ready == 0x01)																	//Wait until the RX buffer has fully received a packet
					{
						status.state = DATAbus_STATE_VALIDATING_RESPONSE;							//Change states to validate the response
					}
			break;
			case DATAbus_STATE_VALIDATING_RESPONSE:	
					if(bus_Slave_Validate_Packet() == WRITE_RESPONSE)
					{
						if(data_to_send)
						{
							status.state = DATAbus_STATE_BROADCAST_TABLE;
						}
						else
						{
							status.state = DATAbus_STATE_SEND_PACKET;
						}
					}
					else
					{
						status.state = DATAbus_STATE_AWAITING_RESPONSE;
					}
					
					bus_Slave_RX_Buffer_Reset();
			break;
			case DATAbus_STATE_BROADCAST_TABLE:
				if(DATAbus_Response_Delay >= DATABUS_RESPONSE_DELAY)
				{
					DATAbus_Response_Delay = 0;
						
					bus_Slave_Send_Broadcast_Response(&control_packet);
				
					status.timeout_counter = 0;																		//Clear the timeout counter
					status.state = DATAbus_STATE_IDLE;														//Change to the next state to start another transmission

					GPIOC->ODR &= ~0x00000200;				//Turn off status led to indicate end of turn/activity					
				}
				else
				{
					DATAbus_Response_Delay++;
				}
			break;
			case DATAbus_STATE_TIMEOUT:
				status.state = DATAbus_STATE_IDLE;
			break;
			default:
				
			break;	
		}
	DATAbus_skip = 0;
	}
}

uint8_t bus_Slave_Validate_Packet(void)
{
	uint16_t RX_Read_Pos = 0;
	uint16_t i = 0;
	
	if(DATAbus_RX_Ready == 0x00)
	{
		DATAbus_Validate_Active = DATAbus_RX_Buffer_Active;
		
		bus_Slave_RX_Buffer_Reset();
		
		while(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] != 0x55 && i < 10)
		{
			RX_Read_Pos++;
		}
		
		if(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] == 0x55)	//Validate the beginning of the packet
		{
			RX_Read_Pos++;
			increment_Data(1, BUS_MESSAGE_RX_COUNT_L_PID); //Increment the RX coutn as we consider this a valid packet
			
			switch(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos])
			{
				case DATAbus_BROADCAST_RESPONSE:
					RX_Read_Pos++;
				
					if(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] == DATAbus_ECU_ID)
					{
						return BROADCAST_RESPONSE;
					}
					else
					{
						return ECU_MISMATCH;
					}
				case DATAbus_BROADCAST_REQUEST:
					RX_Read_Pos++;
				
					if(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] == DATAbus_ECU_ID)
					{
						return BROADCAST_REQUEST;
					}
					else
					{
						return ECU_MISMATCH;
					}
				case DATAbus_WRITE_REQUEST:
					RX_Read_Pos++;
				
					if(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] == DATAbus_ECU_ID)
					{
						return WRITE_REQUEST;
					}
					else
					{
						return ECU_MISMATCH;
					}
				case DATAbus_WRITE_RESPONSE:
					RX_Read_Pos++;
				
					if(DATAbus_RX_Buffer[DATAbus_Validate_Active][RX_Read_Pos] == DATAbus_ECU_ID)
					{
						return WRITE_RESPONSE;
					}
					else
					{
						return ECU_MISMATCH;
					}
				default:
					return INVALID_PACKET;
			}				
		}
		else
		{
			return INVALID_PACKET;
		}				
	}
	
	return INVALID_PACKET;
}

void bus_Slave_RX_Buffer_Add(uint8_t data)
{
	RX_timeout = 0;				//CLEAR the receive timeout counter
	
	DATAbus_RX_Buffer[DATAbus_RX_Buffer_Active][DATAbus_RX_Pos] = data;		//Add the data to the buffer
	DATAbus_RX_Pos++;
	DATAbus_RX_Buffer[DATAbus_RX_Buffer_Active][DATAbus_RX_Pos] = 0;			//Clear the next cell in the recieve buffer
	
	if(DATAbus_RX_Pos == 5)
	{
		DATAbus_RX_len = (DATAbus_RX_Buffer[DATAbus_RX_Buffer_Active][DATAbus_RX_Pos - 2] << 8) | DATAbus_RX_Buffer[DATAbus_RX_Buffer_Active][DATAbus_RX_Pos - 1]; //Extract the length from the packet 
	}
	
	if(DATAbus_RX_Pos >= (DATAbus_RX_len + 5))	//Check to see if all of the data has been received
	{
		DATAbus_RX_Ready = 1;	//Set the flag if all of the data has been received
	}
}

void bus_Slave_RX_Buffer_Reset(void)
{
	RX_timeout = 0;				//CLEAR the receive timeout counter
	
	DATAbus_RX_Buffer_Active++;
	DATAbus_RX_Buffer_Active %= DATABUS_BUFFER_COUNT;
	
	DATAbus_RX_Buffer[DATAbus_RX_Buffer_Active][0] = 0;			//Clear the first cell in the recieve buffer
	
	DATAbus_RX_Pos = 0;
	DATAbus_RX_Ready = 0;
	DATAbus_RX_len = 0;
}

void bus_Slave_Send_Broadcast_Request(uint8_t ECU_ID)
{
	control_packet.start_char = 0x55;			//Assign default start char
	control_packet.control_char = DATAbus_BROADCAST_REQUEST;		//Assign broadcast request
	control_packet.ECU_ID = ECU_ID;				//Assign ECU_ID
	control_packet.len_H = 0;							//Assign empty packet
	control_packet.len_L = 0;							//Assign empty packet
	
	control_packet.checksum = control_packet.start_char + control_packet.control_char + control_packet.ECU_ID + control_packet.len_H + control_packet.len_L; //Calculate the checksum
	
	send_Next_DATAbus_TX(control_packet);
}

void bus_Slave_Send_Broadcast_Response(struct DATAbus_PACKET* packet)
{
	uint16_t temp_len = 0;
	
	control_packet.start_char = 0x55;			//Assign default start char
	control_packet.control_char = DATAbus_BROADCAST_RESPONSE;		//Assign write response
	control_packet.ECU_ID = DATAbus_ECU_ID;				//Assign ECU_ID
	
	temp_len = transmit_Data(&control_packet);
	
	control_packet.len_H = (temp_len & 0xFF00) >> 8;							//Assign packet lenght
	control_packet.len_L = (temp_len & 0x00FF);						
	
	control_packet.checksum = control_packet.start_char + control_packet.control_char + control_packet.ECU_ID + control_packet.len_H + control_packet.len_L; //Calculate the checksum
	
	send_Next_DATAbus_TX(control_packet);
}

void bus_Slave_Send_Write_Request(struct DATAbus_PACKET* packet)
{
	
	
}

void bus_Slave_Send_Write_Response(uint8_t ECU_ID, uint8_t response)
{
	control_packet.start_char = 0x55;			//Assign default start char
	control_packet.control_char = 0x02;		//Assign write response
	control_packet.ECU_ID = ECU_ID;				//Assign ECU_ID
	control_packet.len_H = 0;							//Assign empty packet
	control_packet.len_L = 1;							//Assign empty packet
	
	control_packet.data[0] = response;		//Assign the reponse data
	
	control_packet.checksum = control_packet.start_char + control_packet.control_char + control_packet.ECU_ID + control_packet.len_H + control_packet.len_L + control_packet.data[0]; //Calculate the checksum
	
	send_Next_DATAbus_TX(control_packet);
}


uint8_t get_DATAbus_Slave_State(void)
{
	return status.state;
}
