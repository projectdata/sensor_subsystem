/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: io.c

*/

#include "io.h"

void IO_init (void)
{
	  //Enable peripheral clocks for various ports and subsystems
		RCC->APB2ENR |=  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPEEN; 
	
		//initializing TIM1, TIM2, TIM3, TIM4
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
		RCC->APB1ENR |= RCC_APB1ENR_TIM2EN | RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM4EN;
	
		//initializing AFIO
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	
		//Set the config and mode bits for Port C bit 9 and 8 so they will
		// be push-pull outputs (up to 50 MHz)
		GPIOC->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8;
		GPIOC->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8;

		GPIOC->CRL |= GPIO_CRL_MODE7;
		GPIOC->CRL &= ~GPIO_CRL_CNF7;
}

void tim_output_init()
{	
	//PA1 used for TIM2_CH2 output - 10us pulse
	GPIOA->CRL |= GPIO_CRL_MODE1;
	GPIOA->CRL &= ~GPIO_CRL_CNF1;

	//PA6 used for TIM3_CH1 output - 10us pulse
	GPIOA->CRL |= GPIO_CRL_MODE6;
	GPIOA->CRL &= ~GPIO_CRL_CNF6;
	
	//PA7 used for TIM3_CH2 output - 10us pulse
	GPIOA->CRH |= GPIO_CRL_MODE7;
	GPIOA->CRH &= ~GPIO_CRL_CNF7;
	
	//PB0 used for TIM3_CH3 output - 10us pulse
	GPIOB->CRL |= GPIO_CRL_MODE0;
	GPIOB->CRL &= ~GPIO_CRL_CNF0;
	
	//PB1 used for TIM3_CH4 output - 10us pulse
	GPIOB->CRL |= GPIO_CRL_MODE1;
	GPIOB->CRL &= ~GPIO_CRL_CNF1;
}

void tim_input_init()
{	
	//PA1 - Ultrasonic 1 Echo - TIM2_CH2 IN
	GPIOA->CRL |=  GPIO_CRL_CNF1_0;												
	GPIOA->CRL &= ~GPIO_CRL_CNF1_1 & ~GPIO_CRL_MODE1;	

	//PA6 - Ultrasonic 2 Echo - TIM3_CH1 IN
	GPIOA->CRL |=  GPIO_CRL_CNF6_0;												
	GPIOA->CRL &= ~GPIO_CRL_CNF6_1 & ~GPIO_CRL_MODE6;

	//PA7 - Ultrasonic 3 Echo - TIM3_CH2 IN
	GPIOA->CRL |=  GPIO_CRL_CNF7_0;												
	GPIOA->CRL &= ~GPIO_CRL_CNF7_1 & ~GPIO_CRL_MODE7;
	
	//PB0 - Ultrasonic 4 Echo - TIM3_CH3 IN
	GPIOB->CRL |=  GPIO_CRL_CNF0_0;												
	GPIOB->CRL &= ~GPIO_CRL_CNF0_1 & ~GPIO_CRL_MODE0;
	
	//PB1 - Ultrasonic 5 Echo - TIM3_CH4 IN
	GPIOB->CRL |=  GPIO_CRL_CNF1_0;												
	GPIOB->CRL &= ~GPIO_CRL_CNF1_1 & ~GPIO_CRL_MODE1;
	
	TIM2->CR1 |= TIM_CR1_CEN;
	TIM3->CR1 |= TIM_CR1_CEN;
}

void TIM5_init()
{
	TIM5->CR1 = 0x0080;											//Set TIM4 as upcounter with preload	
	TIM5->DIER = 0x0001;										//Enable the update interrupt
	TIM5->PSC = 2399;												//Divide clock source by 2400 (10kHz)
	TIM5->ARR = 10000;													//Set TIM4 to generate an interrupt every 1s

	NVIC->ISER[0] |= 0x80000000;				  	//Enable the interrupt
	
	TIM5->CR1 |= 0x0001;										//Enable the timer
}
