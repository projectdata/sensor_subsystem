/** 
Project: Project DATA
Developer: Danielle Janier
Creation Date: 02/05/2018

Subsystem: Sensor Interface
File: main.cpp

*/
#include "io.h"
#include "clocks.h"
#include "sensors.h"
#include "DATAbus_slave.h"
#include "comms_slave.h"
#include "Exceptions.h"

int main()
{
	clock_init();
	IO_init();
	tim1_trigger_init();
	echo_pulse_in();
	bumper_init();
	tilt_init();
	usart_init();
	init_Table();
	init_DATAbus_Slave();
	init_DATAbus_TX_Queue();
	
	while(1)
	{
			tim_output_init();
			test_trigger_pulse();
			tim_input_init();
			read_bumper();
			read_tilt();
			delay(600000);
	}
}

void USART3_IRQHandler(void)
{		
	if(((USART3->SR & 0x00000020) >> 5) == 1)
	{
		bus_Slave_RX_Buffer_Add(USART3->DR);
	}	
}
	
void DMA1_Channel2_IRQHandler(void)
{
	set_TX_Complete_DATAbus_TX();

	DMA1->IFCR |= 0x00000010;									//CLEAR the interrupt flags
}	
	
void TIM4_IRQHandler(void)
{
	update_DATAbus_Slave();
	TIM4->SR = 0x00000000;
}

void TIM5_IRQHandler(void)
{
	TIM5_counter++;
	
	if(TIM5_counter == 15)
	{
		TIM5_flag = 1;
		TIM5_counter = 0;
	}
		
	TIM5->SR = 0x00000000;
}

