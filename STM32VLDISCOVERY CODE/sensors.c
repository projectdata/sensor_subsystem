/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: sensors.c

*/

#include "sensors.h"
#include "Exceptions.h"

void tim1_trigger_init()
{
	//PA8 used for trigger output, 10ms high pulse
	GPIOA->CRH |= GPIO_CRH_MODE8 | GPIO_CRH_CNF8_1;
	GPIOA->CRH &= ~GPIO_CRH_CNF8_0;
}

void echo_pulse_in()
{
		//TIM2_CH2 input capture

		//CC1 is input, IC1 mapped on TI1 (01)
		TIM2->CCMR1 |= TIM_CCMR1_CC1S_0; 
		TIM2->CCMR1 &= ~TIM_CCMR1_CC1S_1;
		
		//CC1P is non-inverted, capture on rising edge of IC1
		TIM2->CCER &= ~TIM_CCER_CC1P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM2->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0 | TIM_SMCR_SMS_2;
		TIM2->SMCR &= ~TIM_SMCR_TS_1 & ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
		
		//used to prevent overflow
		TIM2->PSC = 23;
		
		TIM2->DIER |= TIM_DIER_CC1IE;
		
		//enable interrupt
		NVIC->ISER[0] |= 0x10000000;
		
		//capture enable
		TIM2->CCER |= TIM_CCER_CC1E; //| TIM_CCER_CC2E;
		
		//TI1 for CH1, CH2, CH3
		TIM2->CR2 |= TIM_CR2_TI1S;
		
/*******************************************************************************/
		
		//TIM3 CH1 through CH4 input capture
		//TIM3->CR1 |= TIM_CR1_CEN;				//counter enable

		//Using IC1-IC4 for input capture for TI1-TI4
		TIM3->CCMR1 |= TIM_CCMR1_CC2S_1 | TIM_CCMR1_CC1S_1;
		TIM3->CCMR1 &= ~TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;

		TIM3->CCMR2 |= TIM_CCMR2_CC3S_1 | TIM_CCMR2_CC4S_1;
		TIM3->CCMR2 &= ~TIM_CCMR2_CC3S_0 | TIM_CCMR2_CC4S_0;

		//inverted, capture on falling edge of IC1-IC4
		TIM3->CCER |= TIM_CCER_CC2P | TIM_CCER_CC1P | TIM_CCER_CC3P | TIM_CCER_CC4P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM3->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0 | TIM_SMCR_SMS_2;
		TIM3->SMCR &= ~TIM_SMCR_TS_1 & ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
		
		//used to prevent overflow
		TIM3->PSC = 0x10;
		
		//CC Interrupt and Update Interrupt Enabled
		TIM3->DIER |= TIM_DIER_CC2IE| TIM_DIER_CC1IE| TIM_DIER_CC3IE | TIM_DIER_CC4IE;
		
		//enable interrupt for TIM3
		NVIC->ISER[0] |= 0x20000000;
		
		//capture enable
		TIM3->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;
}

void TIM2_IRQHandler(void)
{
		//if the sensor becomes disconneted send not detected error
		//if(ECHO1_READ == 0x0)
		//{
		//		add_Error(ERROR_ULTRASONIC_NOT_DETECTED_MAJOR, ERROR_ULTRASONIC_FRONT_NOT_DETECTED_MINOR, ERROR_ERROR_PRIORITY);
		//}
	
		uint32_t echo1;
		static uint32_t buff[5];
		static uint32_t avg;
	
		//storing raw measurements
		set_Data(ECHO1_READ, ULTRASONIC_RAW_1_PID);

		last = current;
		current = ECHO1_READ;
	
		if(ECHO1_READ > 0x9000)
			current = last;
	
		//calculating and storing distance
		//found the equation using excel
		echo1 = (current * 179)/10000;
		echo1 = echo1 - 10;
	
		buff[count2] = echo1;
	
		if(count2 < 4)
			count2++;
		else
			count2 = 0;
		
		for(int i = 0; i < 4; i++)
		{
			avg = buff[i] + avg;
		}
	
		avg = avg/5;
		
		set_Data(avg & 0xFFFF, ULTRASONIC_CALC_1_PID);

		//clear status register
		TIM2->SR = 0x00000000;
}

void TIM3_IRQHandler(void)
{
		uint32_t echo2;
	
		//storing raw measurements
		set_Data(ECHO3_READ, ULTRASONIC_RAW_2_PID);

		//calculating and storing distance
		//found the equation using excel
		echo2 = (ECHO3_READ * 128)/10000;
		echo2 = echo2 - 10;
		set_Data(echo2 & 0xFFFF, ULTRASONIC_CALC_2_PID);

		//clear status register
		TIM3->SR = 0x00000000;
//		}
//		
//		//PA6
//		if(TIM3->CCR2 < 0x2500 && TIM3->CCR2 > 0x300) 
//		{	
//			uint32_t echo3;
//			set_Data(TIM3->CCR2, ULTRASONIC_RAW_3_PID);
//			echo3 = (((120 * TIM3->CCR2) / 10000) - 1);
//			set_Data(echo3 & 0xFFFF, ULTRASONIC_CALC_3_PID);
//		}
//		//PB1
//			if(TIM3->CCR3 < 0x5000 && TIM3->CCR3 > 0x00A0) 
//		{	
//			uint32_t echo4;
//			set_Data(TIM3->CCR3, ULTRASONIC_RAW_4_PID);
//			echo4 = (((123 * TIM3->CCR3) / 10000) - 1);
//			set_Data(echo4 & 0xFFFF, ULTRASONIC_CALC_4_PID);
//		}
//		//PB0
//		if(TIM3->CCR4 < 0x5000 && TIM3->CCR4 > 0x00A0) 
//		{	
//			uint32_t echo5;
//			set_Data(TIM3->CCR4, ULTRASONIC_RAW_5_PID);
//			echo5 = (((123 * TIM3->CCR4) / 10000) - 1);
//			set_Data(echo5 & 0xFFFF, ULTRASONIC_CALC_5_PID);
//		}
//		
//		TIM3->SR = 0x00000000;
}

void bumper_init()
{
		//PD4 - PD7 for Bumper Inputs
		//2 bumpers are used for each side
		GPIOD->CRL |= GPIO_CRL_CNF4_0 | GPIO_CRL_CNF5_0 | GPIO_CRL_CNF6_0 | GPIO_CRL_CNF7_0;
		GPIOD->CRL &= ~GPIO_CRL_CNF4_1 & ~GPIO_CRL_CNF5_1 & ~GPIO_CRL_CNF6_1 & ~GPIO_CRL_CNF7_1;
		GPIOD->CRL &= ~GPIO_CRL_MODE4 & ~GPIO_CRL_MODE5 & ~GPIO_CRL_MODE6 & ~GPIO_CRL_MODE7;
	
		//PB9 used for testing
		GPIOB->CRH |= GPIO_CRH_CNF9_0;
		GPIOB->CRH &= ~GPIO_CRH_CNF9_1 & ~GPIO_CRH_MODE9;
}

void read_bumper()
{
		//if the bumper has been pressed, set true
		if((GPIOB->IDR & GPIO_IDR_IDR9) != 0x0)
		{
				set_Data(1, BUMP_RIGHT_STATUS_PID);
		}
		else
		{
				set_Data(0, BUMP_RIGHT_STATUS_PID);
		}
}

void tilt_init()
{
		//Using PB8 for testing
		GPIOB->CRH |= GPIO_CRH_CNF8_0;
		GPIOB->CRH &= ~GPIO_CRH_CNF8_1 & ~GPIO_CRH_MODE8;		
	
		//PD0 used for sensor
		//GPIOD->CRL |= GPIO_CRL_CNF0_0;
		//GPIOD->CRL &= ~GPIO_CRL_CNF0_1 & ~GPIO_CRL_MODE0;

		//PD1 Tilt Sensor Input 2
		//GPIOD->CRL |= GPIO_CRL_CNF1_1;
		//GPIOD->CRL &= ~GPIO_CRL_CNF1_0 & ~GPIO_CRL_MODE1;
}

void read_tilt()
{
		//if the tilt sensor goes high, vehicle has tilted
		if((GPIOB->IDR & GPIO_IDR_IDR8) != 0x0)
		{
				set_Data(1, TILT_STATUS_1_PID);
		}
		else
		{
				set_Data(0, TILT_STATUS_1_PID);
		}
}

void test_trigger_pulse()
{
	//setting up as a timer base for 10us output pulse
	TIM1->SR = 0;
	//set delay
	TIM1->ARR = 0x0120;  			//10us delay
	//start timer and set GPIO input high
	TIM1->CR1 |= TIM_CR1_CEN;
	
	//SETTING TIM BITS HIGH!!
	GPIOA->ODR |= GPIO_ODR_ODR1 | GPIO_ODR_ODR6;	
	
	//wait until the flag is set (for delay to finish)
	while (!(TIM1->SR & TIM_SR_UIF));
	//turn-off the GPIO output
	GPIOA->BSRR |= GPIO_BSRR_BR1 | GPIO_BSRR_BR6;	
	TIM1->CR1 &= ~TIM_CR1_CEN;
}

void test_echo_pulse()
{
		//TIM2_CH2 input capture

		//CC1 is input, IC1 mapped on TI1 (01)
		TIM2->CCMR1 |= TIM_CCMR1_CC1S_0; 
		TIM2->CCMR1 &= ~TIM_CCMR1_CC1S_1;
		
		//CC1P is non-inverted, capture on rising edge of IC1
		TIM2->CCER &= ~TIM_CCER_CC1P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM2->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0 | TIM_SMCR_SMS_2;
		TIM2->SMCR &= ~TIM_SMCR_TS_1 & ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
		
		//used to prevent overflow
		TIM2->PSC = 23;
		
		TIM2->DIER |= TIM_DIER_CC1IE;
		
		//enable interrupt
		NVIC->ISER[0] |= 0x10000000;
		
		//capture enable
		TIM2->CCER |= TIM_CCER_CC1E; //| TIM_CCER_CC2E;
		
		//TI1 for CH1, CH2, CH3
		TIM2->CR2 |= TIM_CR2_TI1S;
}

