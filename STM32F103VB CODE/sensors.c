/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: sensors.c

*/

#include "sensors.h"

uint8_t ultrasonic_outOfRange_indexes[5];
uint8_t ultrasonic_outOfRange_status[5];

uint8_t ultrasonic_disconnect_indexes[5];
uint8_t ultrasonic_disconnect_status[5];

uint8_t ultrasonic_error_count[5];

void trigger_pulse_1()
{
	//setting up as a timer base for 10us output pulse
	TIM1->SR = 0;
	//set delay
	TIM1->ARR = 0x0500;  			//10us delay
	//start timer and set GPIO input high
	TIM1->CR1 |= TIM_CR1_CEN;
	
	//SETTING TIM BITS HIGH!!
	GPIOA->ODR |= GPIO_ODR_ODR6;
	GPIOD->ODR |= GPIO_ODR_ODR12;
	
	//wait until the flag is set (for delay to finish)
	while (!(TIM1->SR & TIM_SR_UIF));

	//turn-off the GPIO output
	GPIOA->BSRR |= GPIO_BSRR_BR6;
	GPIOD->BSRR |= GPIO_BSRR_BR12;
	
	TIM1->CR1 &= ~TIM_CR1_CEN;
}

void trigger_pulse_2()
{
	//setting up as a timer base for 10us output pulse
	TIM1->SR = 0;
	//set delay
	TIM1->ARR = 0x0500;  			//10us delay
	//start timer and set GPIO input high
	TIM1->CR1 |= TIM_CR1_CEN;
	
	//SETTING TIM BITS HIGH!!
	GPIOA->ODR |= GPIO_ODR_ODR1 | GPIO_ODR_ODR7;
	GPIOD->ODR |= GPIO_ODR_ODR13;
	
	//wait until the flag is set (for delay to finish)
	while (!(TIM1->SR & TIM_SR_UIF));

	//turn-off the GPIO output
	GPIOA->BSRR |= GPIO_BSRR_BR1 | GPIO_BSRR_BR7;
	GPIOD->BSRR |= GPIO_BSRR_BR13;
	
	TIM1->CR1 &= ~TIM_CR1_CEN;
}

void echo_pulse_in()
{
		//TIM2_CH2 input capture

		//CC2 is input
		TIM2->CCMR1 |= TIM_CCMR1_CC2S_0; 
		TIM2->CCMR1 &= ~TIM_CCMR1_CC2S_1;
		
		//CC2P is non-inverted, capture on rising edge of IC1
		TIM2->CCER |= TIM_CCER_CC2P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM2->SMCR |= TIM_SMCR_SMS_2;
		TIM2->SMCR &= ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
	
		TIM2->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1;
		TIM2->SMCR &= ~TIM_SMCR_TS_0;	
		
		//used to prevent overflow
		TIM2->PSC = 2799;
		
		TIM2->DIER |= TIM_DIER_CC2IE;
		
		//enable interrupt
		NVIC->ISER[0] |= 0x10000000;
		
		//capture enable
		TIM2->CCER |= TIM_CCER_CC2E;

/*******************************************************************************/
		
		//TIM3 CH1/CH2 input capture

		//Using IC1, IC2 for input capture
		TIM3->CCMR1 |= TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;
		TIM3->CCMR1 &= ~TIM_CCMR1_CC2S_1 & ~TIM_CCMR1_CC1S_1;

		//inverted, capture on falling edge
		TIM3->CCER |= TIM_CCER_CC2P | TIM_CCER_CC1P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM3->SMCR |= TIM_SMCR_SMS_2;
		TIM3->SMCR &= ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
		
		//used to prevent overflow
		TIM3->PSC = 2779;
		
		//CC Interrupt and Update Interrupt Enabled
		TIM3->DIER |= TIM_DIER_CC2IE| TIM_DIER_CC1IE;
		
		//enable interrupt for TIM3
		NVIC->ISER[0] |= 0x20000000;
		
		//capture enable
		TIM3->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E;
		
/*******************************************************************************/
		
		//TIM4 CH1/CH2 input capture

		//Using IC1, IC2 for input capture
		TIM4->CCMR1 |= TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;
		TIM4->CCMR1 &= ~TIM_CCMR1_CC2S_1 & ~TIM_CCMR1_CC1S_1;

		//inverted, capture on falling edge
		TIM4->CCER |= TIM_CCER_CC2P | TIM_CCER_CC1P;
	
		//Trigger Select is filtered timer input 1 (101)
		//Slave Mode is reset mode - rising edge trigger reinitializes counter (100)
		TIM4->SMCR |= TIM_SMCR_SMS_2;
		TIM4->SMCR &= ~TIM_SMCR_SMS_1 & ~TIM_SMCR_SMS_0;
		
		//used to prevent overflow
		TIM4->PSC = 2779;
		
		//CC Interrupt and Update Interrupt Enabled
		TIM4->DIER |= TIM_DIER_CC2IE| TIM_DIER_CC1IE;
		
		//enable interrupt for TIM4
		NVIC->ISER[0] |= 0x40000000;
		
		//capture enable
		TIM4->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E;
}

void echo_config_1()
{
	//Configuration 1 - uses Filtered Timer Input 1
	
	TIM4->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0;
	TIM4->SMCR &= ~TIM_SMCR_TS_1;
	
	TIM3->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_0;
	TIM3->SMCR &= ~TIM_SMCR_TS_1;
}

void echo_config_2()
{
	//Configuration 2 - uses Filtered Timer Input 2
	
	TIM4->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1;
	TIM4->SMCR &= ~TIM_SMCR_TS_0;
	
	TIM3->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1;
	TIM3->SMCR &= ~TIM_SMCR_TS_0;	
}

void read_bumper()
{
		//if the bumper has been pressed, set true
		if((GPIOD->IDR & GPIO_IDR_IDR4) != 0x0)
		{
				set_Data(1, BUMP_RIGHT_STATUS_PID);
		}
		else
		{
				set_Data(0, BUMP_RIGHT_STATUS_PID);
		}
		
		if((GPIOD->IDR & GPIO_IDR_IDR5) != 0x0)
		{
				set_Data(1, BUMP_FRONT_STATUS_PID);
		}
		else
		{
				set_Data(0, BUMP_FRONT_STATUS_PID);
		}
		
		if((GPIOD->IDR & GPIO_IDR_IDR6) != 0x0)
		{
				set_Data(1, BUMP_LEFT_STATUS_PID);
		}
		else
		{
				set_Data(0, BUMP_LEFT_STATUS_PID);
		}
		
		if((GPIOD->IDR & GPIO_IDR_IDR7) != 0x0)
		{
				set_Data(1, BUMP_BACK_STATUS_PID);
		}
		else
		{
				set_Data(0, BUMP_BACK_STATUS_PID);
		}		
}

void read_tilt()
{
		//if the tilt sensor goes high, vehicle has tilted
		if((GPIOD->IDR & GPIO_IDR_IDR0) != 0x0)
		{
				set_Data(1, TILT_STATUS_1_PID);
		}
		else
		{
				set_Data(0, TILT_STATUS_1_PID);
		}
		
		if((GPIOD->IDR & GPIO_IDR_IDR0) != 0x0)
		{
				set_Data(1, TILT_STATUS_2_PID);
		}
		else
		{
				set_Data(0, TILT_STATUS_2_PID);
		}
			
}

void TIM2_IRQHandler(void)
{
		uint32_t echo;
		uint16_t ECHO4_READ = TIM2->CCR2;
	
		int calc_calib = get_Data(ULTRASONIC_CALC_CALIB_PID);
		float raw_calib = get_Data(ULTRASONIC_RAW_CALIB_PID);
	
		//storing raw measurements
		set_Data(ECHO4_READ, ULTRASONIC_RAW_4_PID);
	
		/***** Error Checking *****/
		if(ECHO4_READ > ULTRASONIC_DISCONNECT_HIGH || ECHO4_READ < ULTRASONIC_DISCONNECT_LOW)
		{
			if(ultrasonic_error_count[3] >= 40)
			{
				throwDisconnectErrorUltrasonic(4);
			}
		}
		else if(ECHO4_READ > ULTRASONIC_OUT_OF_RANGE)
		{
			if(ultrasonic_error_count[3] >= 40)
			{
				throwOutOfRangeErrorUltrasonic(4);
			}
		}
		else if(ECHO4_READ > ULTRASONIC_MAX_RANGE)
		{
			ECHO4_READ = ULTRASONIC_MAX_RANGE;
			ultrasonic_error_count[3] = 0;
			clearOutOfRangeErrorUltrasonic(4);
			clearDisconnectErrorUltrasonic(4);
		}
		else
		{
			ultrasonic_error_count[3] = 0;
			clearOutOfRangeErrorUltrasonic(4);
			clearDisconnectErrorUltrasonic(4);
		}
		
		//calculating and storing distance
		echo = ECHO4_READ * ULTRASONIC_RAW_SCALING_CALIB;
	
		//object is too close so setting it to get 0 distance	
		if(echo < calc_calib)
		{
			echo = calc_calib;
		}
	
		//subtract calibration value from calculated
		echo = echo - calc_calib;
		
		//send the calculated value to the data table
		set_Data(echo & 0xFFFF, ULTRASONIC_CALC_4_PID);

		//clear status register
		TIM2->SR = 0x00000000;
}

void TIM3_IRQHandler(void)
{	
		uint32_t echo;
	
		int calc_calib = get_Data(ULTRASONIC_CALC_CALIB_PID);
		float raw_calib = get_Data(ULTRASONIC_RAW_CALIB_PID);
	
		if(TIM3->SR & TIM_SR_CC1IF)
		{
			uint16_t ECHO5_READ = TIM3->CCR1;
			
			//TIM3_CH1 measurements
			//storing raw measurements
			set_Data(ECHO5_READ, ULTRASONIC_RAW_5_PID);
			
			if(ECHO5_READ > ULTRASONIC_DISCONNECT_HIGH || ECHO5_READ < ULTRASONIC_DISCONNECT_LOW)
			{
				if(ultrasonic_error_count[4] >= 50)
				{
					throwDisconnectErrorUltrasonic(5);
				}
			}
			else if(ECHO5_READ > ULTRASONIC_OUT_OF_RANGE)
			{
				if(ultrasonic_error_count[4] >= 50)
				{
					throwOutOfRangeErrorUltrasonic(5);
				}
			}
			else if(ECHO5_READ > ULTRASONIC_MAX_RANGE)
			{
				//ECHO5_READ = ULTRASONIC_MAX_RANGE;
				ultrasonic_error_count[4] = 0;
				clearOutOfRangeErrorUltrasonic(5);
				clearDisconnectErrorUltrasonic(5);
			}
			else
			{
				ultrasonic_error_count[4] = 0;
				clearOutOfRangeErrorUltrasonic(5);
				clearDisconnectErrorUltrasonic(5);
			}
			//calculating and storing distance
			//found the equation using excel
			echo = ECHO5_READ * ULTRASONIC_RAW_SCALING_CALIB;
		
			//object is too close so setting it to get 0 distance
			if(echo < calc_calib)
			{
				echo = calc_calib;
			}
		
			echo = echo - calc_calib;
			set_Data(echo & 0xFFFF, ULTRASONIC_CALC_5_PID);
		}
	
		if(TIM3->SR & TIM_SR_CC2IF)
		{
			uint16_t ECHO1_READ = TIM3->CCR2;
			//storing raw measurements
			set_Data(ECHO1_READ, ULTRASONIC_RAW_1_PID);

			if(ECHO1_READ > ULTRASONIC_DISCONNECT_HIGH || ECHO1_READ < ULTRASONIC_DISCONNECT_LOW)
			{
				if(ultrasonic_error_count[0] >= 50)
				{
					throwDisconnectErrorUltrasonic(1);
				}
			}
			else if(ECHO1_READ > ULTRASONIC_OUT_OF_RANGE)
			{
				if(ultrasonic_error_count[0] >= 50)
				{
					throwOutOfRangeErrorUltrasonic(1);
				}
			}
			else if(ECHO1_READ > ULTRASONIC_MAX_RANGE)
			{
				ECHO1_READ = ULTRASONIC_MAX_RANGE;
				ultrasonic_error_count[0] = 0;
				clearOutOfRangeErrorUltrasonic(1);
				clearDisconnectErrorUltrasonic(1);
			}
			else
			{
				ultrasonic_error_count[0] = 0;
				clearOutOfRangeErrorUltrasonic(1);
				clearDisconnectErrorUltrasonic(1);
			}
			//calculating and storing distance
			//found the equation using excel
			echo = ECHO1_READ * ULTRASONIC_RAW_SCALING_CALIB;
		
			//object is too close so setting it to get 0 distance
			if(echo < calc_calib)
			{
				echo = calc_calib;
			}
		
			echo = echo - calc_calib;
			set_Data(echo & 0xFFFF, ULTRASONIC_CALC_1_PID);			
		}
		//clear status register
		TIM3->SR = 0x00000000;
}

void TIM4_IRQHandler(void)
{
		uint32_t echo;
	
		int calc_calib = get_Data(ULTRASONIC_CALC_CALIB_PID);
		float raw_calib = get_Data(ULTRASONIC_RAW_CALIB_PID);
	
		//TIM4_CH1 measurements
		//storing raw measurements
		if(TIM4->SR & TIM_SR_CC1IF)
		{
			uint16_t ECHO3_READ = TIM4->CCR1;
			
			set_Data(ECHO3_READ, ULTRASONIC_RAW_3_PID);

			if(ECHO3_READ > ULTRASONIC_DISCONNECT_HIGH || ECHO3_READ < ULTRASONIC_DISCONNECT_LOW)
			{
				if(ultrasonic_error_count[2] >= 50)
				{
					throwDisconnectErrorUltrasonic(3);
				}
			}
			else if(ECHO3_READ > ULTRASONIC_OUT_OF_RANGE)
			{
				if(ultrasonic_error_count[2] >= 50)
				{
					throwOutOfRangeErrorUltrasonic(3);
				}
			}
			else if(ECHO3_READ > ULTRASONIC_MAX_RANGE)
			{
				ECHO3_READ = ULTRASONIC_MAX_RANGE;
				ultrasonic_error_count[2] = 0;
				clearOutOfRangeErrorUltrasonic(3);
				clearDisconnectErrorUltrasonic(3);
			}
			else
			{
				ultrasonic_error_count[2] = 0;
				clearOutOfRangeErrorUltrasonic(3);
				clearDisconnectErrorUltrasonic(3);
			}
			//calculating and storing distance
			//found the equation using excel
			echo = ECHO3_READ * ULTRASONIC_RAW_SCALING_CALIB;
		
			//object is too close so setting it to get 0 distance
			if(echo < calc_calib)
			{
				echo = calc_calib;
			}
		
			echo = echo - calc_calib;
			set_Data(echo & 0xFFFF, ULTRASONIC_CALC_3_PID);
		}

		if(TIM4->SR & TIM_SR_CC2IF)
		{		
			
			uint16_t ECHO2_READ = TIM4->CCR2;
			//storing raw measurements
			set_Data(ECHO2_READ, ULTRASONIC_RAW_2_PID);

			if(ECHO2_READ > ULTRASONIC_DISCONNECT_HIGH || ECHO2_READ < ULTRASONIC_DISCONNECT_LOW)
			{
				if(ultrasonic_error_count[1] >= 50)
				{
					throwDisconnectErrorUltrasonic(2);
				}
			}
			else if(ECHO2_READ > ULTRASONIC_OUT_OF_RANGE)
			{
				if(ultrasonic_error_count[1] >= 50)
				{
					throwOutOfRangeErrorUltrasonic(2);
				}
			}
			else if(ECHO2_READ > ULTRASONIC_MAX_RANGE)
			{
				ECHO2_READ = ULTRASONIC_MAX_RANGE;
				ultrasonic_error_count[1] = 0;
				clearOutOfRangeErrorUltrasonic(2);
				clearDisconnectErrorUltrasonic(2);
			}
			else
			{
				ultrasonic_error_count[1] = 0;
				clearOutOfRangeErrorUltrasonic(2);
				clearDisconnectErrorUltrasonic(2);
			}
			//calculating and storing distance
			//found the equation using excel
			echo = ECHO2_READ * ULTRASONIC_RAW_SCALING_CALIB;
		
			//object is too close so setting it to get 0 distance
			if(echo < calc_calib)
			{
				echo = calc_calib;
			}
			
			echo = echo - calc_calib;
			set_Data(echo & 0xFFFF, ULTRASONIC_CALC_2_PID);			
		}
		//clear status register
		TIM4->SR = 0x00000000;	
}


/***** Throwing and Clearing Errors *****/
void throwOutOfRangeErrorUltrasonic(uint8_t input)
{
	if(ultrasonic_outOfRange_status[input] == 0)
	{
		ultrasonic_outOfRange_status[input] = 1;
		ultrasonic_outOfRange_indexes[input] = add_Error(ERROR_ULTRASONIC_OUT_OF_RANGE_MAJOR, input, ERROR_WARNING_PRIORITY);
	}
}

void clearOutOfRangeErrorUltrasonic(uint8_t input)
{
		if(ultrasonic_outOfRange_status[input] == 1)
	{
		ultrasonic_outOfRange_status[input] = 0;
		remove_Error(ultrasonic_outOfRange_indexes[input]);
	}
}

void throwDisconnectErrorUltrasonic(uint8_t input)
{
	if(ultrasonic_disconnect_status[input] == 0)
	{
		ultrasonic_disconnect_status[input] = 1;
		ultrasonic_disconnect_indexes[input] = add_Error(ERROR_ULTRASONIC_NOT_DETECTED_MAJOR, input, ERROR_WARNING_PRIORITY);
	}
}

void clearDisconnectErrorUltrasonic(uint8_t input)
{
		if(ultrasonic_disconnect_status[input] == 1)
	{
		ultrasonic_disconnect_status[input] = 0;
		remove_Error(ultrasonic_disconnect_indexes[input]);
	}
}

void increment_counter()
{
	for(int i = 0; i < 5; i++)
		ultrasonic_error_count[i]++;
}
