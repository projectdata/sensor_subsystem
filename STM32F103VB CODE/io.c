/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: io.c

*/

#include "io.h"

void IO_init (void)
{
	//Full remap of TIM1 pins
	//remapped to using PE7-PE15
	//will be using PE9 for trigger output
	
	/**
		Full remap of TIM1 and TIM4 pins
			TIM1 : PE7 - PE15 
			TIM4 : PD12 - PD15
	*/
	AFIO->MAPR |= AFIO_MAPR_TIM1_REMAP | AFIO_MAPR_TIM4_REMAP;

	//initializing the status LEDs on PD8 and PD9
	GPIOD->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8;
	GPIOD->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8;
}

void tim_output_init()
{
	//PA6 used for TIM3_CH1 output - 10us pulse
	GPIOA->CRL |= GPIO_CRL_MODE6;
	GPIOA->CRL &= ~GPIO_CRL_CNF6;
	
	//PD12 used for TIM4_CH1 output - 10us pulse
	GPIOD->CRH |= GPIO_CRH_MODE12;
	GPIOD->CRH &= ~GPIO_CRH_CNF12;
	
	//PA1 used for TIM2_CH2 output - 10us pulse
	GPIOA->CRL |= GPIO_CRL_MODE1;
	GPIOA->CRL &= ~GPIO_CRL_CNF1;
	
	//PA7 used for TIM3_CH2 output - 10us pulse
	GPIOA->CRL |= GPIO_CRL_MODE7;
	GPIOA->CRL &= ~GPIO_CRL_CNF7;
	
	//PD13 used for TIM4_CH2 output - 10us pulse
	GPIOD->CRH |= GPIO_CRH_MODE13;
	GPIOD->CRH &= ~GPIO_CRH_CNF13;
}

void tim_input_init()
{	
	//Setting all as pull-down inputs
	//have to set the PxODR to 0 for pull-down resistor
	
	//PA6 - Ultrasonic 2 Echo - TIM3_CH1 IN
	GPIOA->CRL |=  GPIO_CRL_CNF6_1;												
	GPIOA->CRL &= ~GPIO_CRL_CNF6_0 & ~GPIO_CRL_MODE6;

	GPIOA->ODR &= ~GPIO_ODR_ODR6;
	
	//PD12 - Ultrasonic 4 Echo - TIM4_CH1 IN
	GPIOD->CRH |= GPIO_CRH_CNF12_1;
	GPIOD->CRH &= ~GPIO_CRH_CNF12_0 & ~GPIO_CRH_MODE12;

	GPIOD->ODR &= ~GPIO_ODR_ODR12;	
	
	//PA1 - Ultrasonic 1 Echo - TIM2_CH2 IN
	GPIOA->CRL |=  GPIO_CRL_CNF1_1;												
	GPIOA->CRL &= ~GPIO_CRL_CNF1_0 & ~GPIO_CRL_MODE1;	
	
	GPIOA->ODR &= ~GPIO_ODR_ODR1;	
	
	//PA7 - Ultrasonic 3 Echo - TIM3_CH2 IN
	GPIOA->CRL |=  GPIO_CRL_CNF7_1;												
	GPIOA->CRL &= ~GPIO_CRL_CNF7_0 & ~GPIO_CRL_MODE7;

	GPIOA->ODR &= ~GPIO_ODR_ODR7;

	//PD13 - Ultrasonic 5 Echo - TIM4_CH2 IN
	GPIOD->CRH |= GPIO_CRH_CNF13_1;
	GPIOD->CRH &= ~GPIO_CRH_CNF13_0 & ~GPIO_CRH_MODE13;
	
	GPIOD->ODR &= ~GPIO_ODR_ODR13;	
	
	//enabling the timers to get echo input
	TIM2->CR1 |= TIM_CR1_CEN;
	TIM3->CR1 |= TIM_CR1_CEN;
	TIM4->CR1 |= TIM_CR1_CEN;
}

void TIM1_init()
{
	//PE9 used for trigger output, 10ms high pulse
	GPIOE->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_CNF9_1;
	GPIOE->CRH &= ~GPIO_CRH_CNF9_0;
}

void bumper_init()
{
		//PD4 - PD7 for bumper pull-up input
		//2 bumpers are used for each side
		GPIOD->CRL |= GPIO_CRL_CNF4_1 | GPIO_CRL_CNF5_1 | GPIO_CRL_CNF6_1 | GPIO_CRL_CNF7_1;
		GPIOD->CRL &= ~GPIO_CRL_CNF4_0 & ~GPIO_CRL_CNF5_0 & ~GPIO_CRL_CNF6_0 & ~GPIO_CRL_CNF7_0;
		GPIOD->CRL &= ~GPIO_CRL_MODE4 & ~GPIO_CRL_MODE5 & ~GPIO_CRL_MODE6 & ~GPIO_CRL_MODE7;

		GPIOD->ODR |= GPIO_ODR_ODR4 | GPIO_ODR_ODR5 | GPIO_ODR_ODR6 | GPIO_ODR_ODR7; 
}

void tilt_init()
{	
		//PD0 used for sensor
		GPIOD->CRL |= GPIO_CRL_CNF0_0;
		GPIOD->CRL &= ~GPIO_CRL_CNF0_1 & ~GPIO_CRL_MODE0;

		//PD1 Tilt Sensor Input 2
		GPIOD->CRL |= GPIO_CRL_CNF1_1;
		GPIOD->CRL &= ~GPIO_CRL_CNF1_0 & ~GPIO_CRL_MODE1;
}


