/** 
Project: Project DATA
Developer: Danielle Janier

Subsytem: Sensor Interface
File: Exeptions.c

*/

#include "Exceptions.h"

uint8_t add_Error(uint16_t majorcode, uint8_t minorcode, uint8_t priority)
{
	uint32_t error_status = get_Data(ACTIVE_ERRORS_STATUS_L_PID) | (get_Data(ACTIVE_ERRORS_STATUS_H_PID) << 16);
	
	uint16_t error_L = majorcode;
	uint16_t error_H = minorcode | (priority << 14);
	
	int i = 0;
	bool match = false;
	
	for(i = 0; i < 30; i++)	//Check to see if the error already exists in the table
	{
		uint16_t tempmajor = get_Data(ERROR_CODE_VECTOR_BASE_PID + (i * 2));
		uint16_t tempminor = get_Data(ERROR_CODE_VECTOR_BASE_PID + (i * 2) + 1);
		
		if(tempmajor == error_L && (tempminor & 0xC00F) == error_H)
		{
			match = true;
			break;
		}
	}
	
	if(match == false)
	{
		i = 0;
		
		while(((error_status >> i) & 0x01) == 0x01)
		{
			i++;
			
			if(i > 30)
			{
				i = 0;
				break;
			}	
		}
	}
	
		uint16_t count = (get_Data(ERROR_CODE_VECTOR_BASE_PID + (i * 2) + 1) & 0x3FF0) >> 4;
		count++;
		
		error_status |= (0x01 << i);
		
		set_Data(error_L, ERROR_CODE_VECTOR_BASE_PID + (i * 2));
		set_Data(error_H | (count << 4), ERROR_CODE_VECTOR_BASE_PID + (i * 2) + 1);
		
		set_Data(error_status & 0x0000FFFF, ACTIVE_ERRORS_STATUS_L_PID);
		set_Data((error_status >> 16) & 0x0000FFFF, ACTIVE_ERRORS_STATUS_H_PID);
	
	return i;
}

void remove_Error(uint8_t position)
{
	uint32_t error_status = get_Data(ACTIVE_ERRORS_STATUS_L_PID) | (get_Data(ACTIVE_ERRORS_STATUS_H_PID) << 16);
	
	error_status &= ~(0x00000001 << position);	//Clear the active flag from the table
	
	set_Data(error_status & 0x0000FFFF, ACTIVE_ERRORS_STATUS_L_PID);
	set_Data((error_status >> 16) & 0x0000FFFF, ACTIVE_ERRORS_STATUS_H_PID);
	
}
