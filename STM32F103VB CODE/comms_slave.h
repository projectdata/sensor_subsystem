/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: comms_slave.h

*/

#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"
#include "stm32f10x.h"
#include "DATAbus_slave.h"

#endif

#ifndef COMMS_H
#define COMMS_H

#define MAX_DATA_SIZE 1050
#define PC_TX_SIZE 8
#define DATAbus_TX_SIZE 8
#define MAX_ECHO 1000
#define MAX_DATAbus_PACKET 1050

typedef struct queue_Node
{
	uint16_t len;
	uint32_t address;
}queue_Node;

typedef struct circ_Queue
{
	uint8_t TX_READY;
	uint8_t COPY_READY;
	uint8_t IS_FULL;
	uint8_t IS_EMPTY;
	uint8_t head;
	uint8_t tail;

	queue_Node nodes[PC_TX_SIZE];
}circ_Queue;

/**
	@param None
	
	@return None

	@brief Initializes USART3 to use with DATAbus communication protocol
*/
void comms_init(void);

/**
	@param A DATAbus packet that is to be sent next
	
	@return None

	@brief Sends the next item (if it exists) on the PC TX Queue
*/
void send_Next_DATAbus_TX(struct DATAbus_PACKET packet);

/**
	@param None
	
	@return None
	
	@brief Indicates that the TX is complete and the DMA is ready to transmit
				 the next item on the queue		 
*/
void set_TX_Complete_DATAbus_TX(void);

/**
	@param None
	
	@return None
	
	@brief Initilaizes the PC_TX_Queue
*/
void init_DATAbus_TX_Queue(void);

#endif
