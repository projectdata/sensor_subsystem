/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: data.h

*/

#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"
#include "stm32f10x.h"
#include "DATAbus_slave.h"
#include "comms_slave.h"

#endif

#ifndef DATA_H
#define DATA_H

#include "data_table_PIDs.h"
#include "table_init.h"

#define TABLE_SIZE  (uint16_t)512

/**
The global controller data table for
*/

#define ECU_TABLE_OVERHEAD 10
#define MAX_ADDRESS 125
#define PACKET_SIZE 50

struct ECU_TABLE
{
	uint16_t controlchar;
	uint16_t ECU_ID;
	uint16_t start;
	uint16_t len;
	uint16_t data_table[TABLE_SIZE];
	uint16_t check;
};

/** 
	@param None
	
	@return None
	
	@brief Populates the data table with initial values
*/
void init_Table(void);

/**
	@param PID location
	
	@return 16 bit data
	
	@brief Gets data from the table at the specified PID 
*/
uint16_t get_Data(uint16_t PID);

/**
	@param 16 bit data
	
	@param PID location
	
	@return None
	
	@brief Puts data into the table at the specifed PID
*/
void set_Data(uint16_t data, uint16_t PID);

/**
	@param indicates rollover
	
	@param PID location
	
	@return None
	
	@brief Increments the data at the specified PID, overflows into PID + 1 if rollover != 0
*/
void increment_Data(uint8_t rollover, uint16_t PID);

/**
	@param DATAbus packet
	
	@return 16 bit data
	
	@brief Places data from the table into the DATAbus packet
*/
uint16_t transmit_Data(struct DATAbus_PACKET* packet);

#endif
