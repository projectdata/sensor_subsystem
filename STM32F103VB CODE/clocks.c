/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: clocks.c

*/

#include "clocks.h"

void port_clock_init()
{
		//Enable peripheral clocks for various ports and subsystems
		RCC->APB2ENR |=  RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPEEN; 
	
		//initializing TIM1, TIM2, TIM3, TIM4
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
		RCC->APB1ENR |= RCC_APB1ENR_TIM2EN | RCC_APB1ENR_TIM3EN | RCC_APB1ENR_TIM4EN;
	
		//initializing USART2
		RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
	
		//initializing AFIO
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
}

