/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: sensors.h

*/

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include "stm32f10x.h"

#endif

#ifndef SENSORS_H
#define SENSORS_H

#include "data.h"
#include "Exceptions.h"

//#define ECHO5_READ			(uint16_t)TIM3->CCR1

static const float ULTRASONIC_RAW_SCALING_CALIB = 0.65;
static const int ULTRASONIC_CALC_CALIB = 7;
static const int ULTRASONIC_DISCONNECT_HIGH = 1000;
static const int ULTRASONIC_DISCONNECT_LOW = 10;
static const int ULTRASONIC_MAX_RANGE = 500;
static const int ULTRASONIC_OUT_OF_RANGE = 850;

/**
	@param None
	
	@return None
	
	@brief Initializes the ultrasonic GPIO
*/
void ultrasonic_init(void);

/**
	@param None
	
	@return None
	
	@brief Configures the timers for capture compare
*/
void echo_pulse_in(void);

void echo_config_1(void);

void echo_config_2(void);

/**
	@param None
	
	@return None
	
	@brief Sends out a trigger pulse to 3 TIM GPIO
*/
void trigger_pulse_1(void);

/**
	@param None
	
	@return None
	
	@brief Sends out a trigger pulse to 3 TIM GPIO
*/
void trigger_pulse_2(void);

/**
	@param None
	
	@return None
	
	@brief Reads the status of the bumper and sends to the DATAbus
*/
void read_bumper(void);

/**
	@param None
	
	@return None
	
	@brief Reads the status of the bumper and sends to the DATAbus
*/
void read_tilt(void);

void throwOutOfRangeErrorUltrasonic(uint8_t input);

void clearOutOfRangeErrorUltrasonic(uint8_t input);

void throwDisconnectErrorUltrasonic(uint8_t input);

void clearDisconnectErrorUltrasonic(uint8_t input);

void increment_counter(void);

#endif
