/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: data_table_PIDs.h

*/

#ifndef DATA_TABLE_H
#define DATA_TABLE_H

//Controller Type and Version
#define CONTROLLER_TYPE_PID 0
#define CONTROLLER_SW_VERS_PID 1
#define CONTROLLER_HW_VERS_PID 2

//Serial Number
#define CONTROLLER_SN_LL_PID 3
#define CONTROLLER_SN_LH_PID 4
#define CONTROLLER_SN_HL_PID 5
#define CONTROLLER_SN_HH_PID 6

//Clock
#define UTC_TIME_L_PID 8
#define UTC_TIME_H_PID 9

//CAN Status
#define BUS_MESSAGE_TX_COUNT_L_PID 10
#define BUS_MESSAGE_TX_COUNT_H_PID 11
#define BUS_MESSAGE_RX_COUNT_L_PID 12
#define BUS_MESSAGE_RX_COUNT_H_PID 13

//Controller Status
#define CONTROLLER_POWER_STATUS_PID 14
#define CONTROLLER_TEMP_STATUS_PID 15

//Error Status
#define ACTIVE_ERRORS_STATUS_L_PID 18
#define ACTIVE_ERRORS_STATUS_H_PID 19

//Error Table
#define ERROR_CODE_VECTOR_BASE_PID 20

/***** Sensor Interface Data Table PIDs *****/

//LED Addresses
#define STATUS_LED_1_PID			100
#define STATUS_LED_2_PID			101

//Ultrasonic Sensor Addresses
#define ULTRASONIC_BASE_PID		102
#define	ULTRASONIC_RAW_1_PID	102
#define	ULTRASONIC_CALC_1_PID	103
#define	ULTRASONIC_RAW_2_PID	104
#define	ULTRASONIC_CALC_2_PID	105
#define	ULTRASONIC_RAW_3_PID	106
#define	ULTRASONIC_CALC_3_PID	107
#define	ULTRASONIC_RAW_4_PID	108
#define	ULTRASONIC_CALC_4_PID	109
#define	ULTRASONIC_RAW_5_PID	110
#define	ULTRASONIC_CALC_5_PID	111

//Bump Sensor Addresses
#define BUMP_BASE_PID					112
#define BUMP_RIGHT_STATUS_PID	112
#define BUMP_LEFT_STATUS_PID	114
#define BUMP_FRONT_STATUS_PID 116
#define BUMP_BACK_STATUS_PID	118

//Tilt Sensor Addresses
#define TILT_BASE_PID					120
#define TILT_STATUS_1_PID			120
#define TILT_STATUS_2_PID			122

//Ultrasonic Calibrations
#define ULTRASONIC_RAW_CALIB_PID  124
#define ULTRASONIC_CALC_CALIB_PID 125


#endif
