/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: clocks.h

*/

#include <stdint.h>
#include "stm32f10x.h"

/**
	@params None
	
	@return None
	
	@brief Initializes the GPIO ports, timers, AFIO clocks
*/
void port_clock_init(void);
