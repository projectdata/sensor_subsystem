/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: comms.c

*/

#include "comms_slave.h"
#include "data_table_PIDs.h"
#include "data.h"

circ_Queue DATAbus_TX_QUEUE;

void comms_init()
{	
	GPIOA->CRL |= 0x00004B00;								//Set PA2 as Alternate Function Push/Pull output
	GPIOA->CRL &= 0xFFFF4BFF;								//Set PA3 as floating input

	RCC->AHBENR |= 0x00000001;							//Enable DMA clock
	
	USART2->CR2 |= 0x00000000;								//Reciever interrupts enabled.
	USART2->CR3 |= 0x00000080;
	
	//USART3->BRR = 0x00000271;								//Configure USART baud rate to 19200 with a 24 MHz Clock
	USART2->BRR = 0x000002C2;								//Configure USART baud rate to 50000 with a 72 MHz Clock
	
	NVIC->ISER[1] |= 0x00000040;				  	//Enable the USART interrupt
	
	USART2->CR1 |= 0x0000002C;							//Enable the USART, configure to 8, N, 1 
	USART2->CR1 |= 0x00000008;
	
	//DMA Controller for UART
	DMA1_Channel7->CCR = 0x00002092;				//Configure DMA Channel 2 for 8bit memory to 8 bit perhipheral mode, enable transfer complete interrupt
	DMA1_Channel7->CNDTR = 0;								//SET transfer amount to 0
	DMA1_Channel7->CPAR = USART2_BASE + 0x04;				//SET the perhipheral adress to the address of USART3-DR
	DMA1_Channel7->CMAR = 0;								//Set the memory address to 0
	
	NVIC->ISER[0] |= 0x00020000;						//Enable DMA interrupt
	
	USART2->CR1 |= 0x00002000;
	
	SysTick->CTRL = 0;
	SysTick_Config(SystemCoreClock/200);					 //generate an interrupt every 5ms
  SysTick->VAL = 0;															 //load systick value
  SysTick->CTRL = (SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk) | SysTick_CTRL_CLKSOURCE_Msk;
}

// DATAbus_TX_Queue
void set_TX_Complete_DATAbus_TX(void)
{
	DATAbus_TX_QUEUE.TX_READY = 1;
	USART2->CR1 |= 0x00000004;
}

void send_Next_DATAbus_TX(struct DATAbus_PACKET packet)
{
	while(DATAbus_TX_QUEUE.TX_READY == 0){};								//Wait until the DMA is ready to transmit another
		
	increment_Data(1, BUS_MESSAGE_TX_COUNT_L_PID);		//Increment the number of messages transmitted on the DATAbus
		
	DATAbus_TX_QUEUE.TX_READY = 0;								//Indicate that another transfer cannot start;
		
	while((USART2->SR & 0x00000040) != 0x00000040){};			//Wait until USART transmission has completed
	
	DMA1_Channel7->CCR = 0;											//Disable the DMA Controller
		
	DMA1_Channel7->CPAR = USART2_BASE + 0x04;		//SET the USART data regisiter address
	DMA1_Channel7->CMAR = (uint32_t)&packet;					//Set the data packet to be copied from
	DMA1_Channel7->CNDTR = 5 + ((packet.len_H << 8 ) | packet.len_L);								//Set the number of bytes to be transferred
	DMA1_Channel7->CCR = 0x00002092;
		
	USART2->CR1 &= ~0x00000004;
		
	USART2->SR &= 0xFFFFFFBF;										//CLEAR TC flag
		
	DMA1_Channel7->CCR |= 0x00000001;						//Start the DMA transfer	
}

void init_DATAbus_TX_Queue(void)
{
	DATAbus_TX_QUEUE.head = 0;
	DATAbus_TX_QUEUE.tail = 0;
	DATAbus_TX_QUEUE.COPY_READY = 1;
	DATAbus_TX_QUEUE.TX_READY = 1;
}
