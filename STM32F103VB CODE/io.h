/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: io.h

*/

#include <stdint.h>
#include "stm32f10x.h"

//used as a counts every 10ms
static int SysTick_counter = 0;

/**
	@params None
	
	@return None
	
	@brief Remaps TIM1
				 Configures the status LED ports
*/
void IO_init(void);

/**
	@params None
	
	@return None
	
	@brief Configures the timer ports to be outputs
*/
void tim_output_init(void);

/**
	@params None
	
	@return None
	
	@brief Configures the timer ports to be inputs
				 Enables the timers
*/
void tim_input_init(void);

/**
	@param None
	
	@return None
	
	@brief Initalizes TIM1_CH1 GPIO
*/
void TIM1_init(void);

/**
	@param None
	
	@return None
	
	@brief Initializes the bumper sensors GPIO
*/
void bumper_init(void);

/**
	@param None
	
	@return None
	
	@brief Initializes the tilt sensors GPIO
*/
void tilt_init(void);

