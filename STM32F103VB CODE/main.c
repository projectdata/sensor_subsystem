/** 
Project: Project DATA
Developer: Danielle Janier

Subsystem: Sensor Interface
File: main.c

*/
#include "io.h"
#include "clocks.h"
#include "sensors.h"
#include "DATAbus_slave.h"
#include "comms_slave.h"
#include "Exceptions.h"

int position = 0;
int time_count = 0;

int main()
{
	//initializing everything
	port_clock_init();
	IO_init();
	TIM1_init();
	echo_pulse_in();
	bumper_init();
	tilt_init();
	comms_init();
	init_Table();
	init_DATAbus_Slave();
	init_DATAbus_TX_Queue();

	//setting the intial calibration values in the data table
	set_Data(ULTRASONIC_RAW_SCALING_CALIB, ULTRASONIC_RAW_CALIB_PID);
	set_Data(ULTRASONIC_CALC_CALIB, ULTRASONIC_CALC_CALIB_PID);
	
	while(1)
	{		
		//doing TIMx_CH1 ultrasonics
		if(SysTick_counter == 10 && position == 0)
		{
			position = 1;
			echo_config_1();			//configuring to capture for CH1
			tim_output_init();		//setting GPIO to output
			trigger_pulse_1();		//send trigger pulse
			tim_input_init();			//setting GPIO to input
			SysTick_counter = 0;	
		}
		//doing TIMx_CH2 ultrasonics
		if(SysTick_counter == 10 && position == 1)
		{
			position = 0;
			echo_config_2();			//configuring to capture for CH2
			tim_output_init();		//setting GPIO to output
			trigger_pulse_2();		//send trigger pulse
			tim_input_init();			//setting GPIO to input
			SysTick_counter = 0;
		}
	
//		read_bumper();				//bump sensors have been moved to the safety system
		read_tilt();						//getting value from tilt sensor
	}
}

void USART2_IRQHandler(void)
{		
	if(((USART2->SR & 0x00000020) >> 5) == 1)
	{
		bus_Slave_RX_Buffer_Add(USART2->DR);
	}	
}
	
void DMA1_Channel7_IRQHandler(void)
{
	set_TX_Complete_DATAbus_TX();

	DMA1->IFCR |= 0x01000000;									//CLEAR the interrupt flags
}	

void SysTick_Handler(void)
{
	update_DATAbus_Slave();
	SysTick_counter++;
	increment_counter();
	
	//ECU time counter
	time_count++;
	if(time_count == 200)
	{
		increment_Data(1, UTC_TIME_L_PID);
		time_count = 0;
	}
}
